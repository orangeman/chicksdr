#include <Arduino.h>
#include <TimeLib.h>
#include "WiFiClientSecure.h"
#include "IPGeolocation.h"
#include "EEPROM.h"
#include "nostr.h"
#include "Sun.h"

const char* ssid     = "< Wifi SSID >";
const char* password = "< Wifi pass >";

int magnetPin1 = 12;
int magnetPin2 = 14;
int direction = 0;
int BACKWARD = 26; // pin
int FORWARD = 25; // pin
long startTime = 0;

void setup() {
  Serial.begin(9600);

  pinMode(FORWARD, OUTPUT);
  pinMode(BACKWARD, OUTPUT);
  pinMode(magnetPin1, INPUT_PULLUP);
  pinMode(magnetPin2, INPUT_PULLUP);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("WiFi connected");
  initNostr(receiveDM);
  geolocateSun();
}

void receiveDM(String msg) {
  Serial.println("DIRECT MESSAGE: " + String(msg));
  if (msg == "O" || msg == "open" || msg == "Open") {
    startMotor(FORWARD);
  }
  if (msg == "C" || msg == "close" || msg == "Close") {
    startMotor(BACKWARD);
  }
}

void startMotor(int dir) {
  direction = dir;
  startTime = millis();
  if (direction == FORWARD) {
    Serial.println("OPENING DOOR");
  } else {
    Serial.println("CLOSING DOOR");
  }
  digitalWrite(direction, HIGH);
}

void stopMotor() {
  Serial.println("Motor STOP");
  digitalWrite(BACKWARD, LOW);
  digitalWrite(FORWARD, LOW);
  direction = 0;
}

bool doorIsClosed() {
  return digitalRead(magnetPin1) == 0;
}

bool doorIsOpen() {
  return digitalRead(magnetPin2) == 0;
}

int TIMEOUT = 60000;

void loop() {

  if (direction == 0) {

    nostrLoop();

  } else {
    long t = millis() - startTime;
    if (direction == BACKWARD && (doorIsClosed() || t > TIMEOUT)) {
      stopMotor();
      sendDM("Magnet 1 CLOSED succesful");
    }
    if (direction == FORWARD && (doorIsOpen() || t > TIMEOUT)) {
      stopMotor();
      sendDM("Magnet 2 CLOSED succesful");
    }
  }
  delay(100);
}

void geolocateSun() {

  EEPROM.begin(16);
  double lat = EEPROM.readDouble(0);
  double lng = EEPROM.readDouble(8);

  if (lat == 0 || isnan(lat) || lng == 0 || isnan(lng)) {
    Serial.println("No stored lat/lng. IP Geolocating...");
    IPGeo IPG;
    IPGeolocation location("ade0fb2687b04a268287f5095b4be852");
    location.updateStatus(&IPG);
    Serial.println("Location is " + IPG.city + ", " + IPG.country + " (" + IPG.tz + ")");
    EEPROM.writeDouble(0, IPG.latitude);
    EEPROM.writeDouble(8, IPG.longitude);
    EEPROM.commit();
    lat = IPG.latitude;
    lng = IPG.longitude;
  }
  Serial.println("Lat/Lng: " + String(lat) + " / " + String(lng) + "\n");

  Sun sun(lat, lng);
  unsigned long tm = 1712753396; // ToDo
  unsigned long rise = sun.getRise(tm);
  unsigned long set = sun.getSet(tm);
  Serial.print("Sun rises at: "); Serial.println(rise);
  printTime(rise);
  Serial.print("Sun set at: "); Serial.println(set);
  printTime(set);
}

void printTime(long t) {
  printf("%4d-%02d-%02d %02d:%02d:%02d\n",
   year(t), month(t), day(t),
    hour(t), minute(t), second(t));
}