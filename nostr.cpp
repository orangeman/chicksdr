#include "time.h"
#include "nostr.h"
#include <string>
#include <NostrEvent.h>
#include <NostrRelayManager.h>
#include <TimeLib.h>


NostrEvent nostr;
NostrRelayManager nostrRelayManager;
NostrQueueProcessor nostrQueue;

bool hasSentEvent = false;

char const *nsecHex = "25119be7e71515e86e13f202c911f91727ec9da3239792f19031edfed39fd3c3"; // sender private key in hex e.g. bdd19cecdXXXXXXXXXXXXXXXXXXXXXXXXXX
char const *npubHex = "4f5399293cac2a873baa166e30865fbe4d8f712151973f97794babc4da152c09"; // sender public key in hex e.g. d0bfc94bd4324f7df2a7601c4177209828047c4d3904d64009a3c67fb5d5e7ca

unsigned long getUnixTimestamp() {
  time_t now;
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return 0;
  } else {
    Serial.println("Got timestamp of " + String(now));
  }
  time(&now);
  return now;
}

Callback callback;

void receiveDM(const std::string& key, const char* payload) {
  String dm = nostr.decryptDm(nsecHex, payload);
  callback(dm);
}

// template<typename Callback>
void initNostr(Callback func) {
  callback = func;
  // NTP server to request epoch time
  const char* ntpServer = "pool.ntp.org";
  const long  gmtOffset_sec = 0;
  const int   daylightOffset_sec = 3600;
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);

  std::vector<String> relays = {
    "nostr.rikles.eu",
    // "nostr.sidnlabs.nl",
    "strfry.openhoofd.nl"
  };
  nostr.setLogging(false);
  nostrRelayManager.setRelays(relays);
  nostrRelayManager.setMinRelaysAndTimeout(2,20000);
  nostrRelayManager.setEventCallback("4", receiveDM);
  nostrRelayManager.connect();

  subscribeDMs();
}

void nostrLoop() {
  nostrRelayManager.loop();
  nostrRelayManager.broadcastEvents();
}

void postNote(String text) {
  long timestamp = getUnixTimestamp();
  String noteString = nostr.getNote(nsecHex, npubHex, timestamp, "Hi from esp32");
  nostrRelayManager.enqueueMessage(noteString.c_str());
}
void sendDM(String msg) {
  long timestamp = getUnixTimestamp();
  String dmString = nostr.getEncryptedDm(nsecHex, npubHex, "b7d7c9d833dba21ba546d3e2ac9d85b46d01dd7ef8bf6d6392033daee3a49107", timestamp, msg);
  nostrRelayManager.enqueueMessage(dmString.c_str());
}

void subscribeDMs() {
  NostrRequestOptions* eventRequestOptions = new NostrRequestOptions();

  int kinds[] = { 4 };
  eventRequestOptions->kinds = kinds;
  eventRequestOptions->kinds_count = sizeof(kinds) / sizeof(kinds[0]);

  String p[] = {"4f5399293cac2a873baa166e30865fbe4d8f712151973f97794babc4da152c09"};
  eventRequestOptions->p = p;
  eventRequestOptions->p_count = sizeof(p) / sizeof(p[0]);

  long t = getUnixTimestamp();
  Serial.println("Now: ");
  printf("%4d-%02d-%02d %02d:%02d:%02d\n",
   year(t), month(t), day(t),
    hour(t), minute(t), second(t));
  eventRequestOptions->since = getUnixTimestamp();
  // eventRequestOptions->since = 1072552928;
  // eventRequestOptions->until = timestamp;
  // eventRequestOptions->limit = 5;

  nostrRelayManager.requestEvents(eventRequestOptions);
  
  delete eventRequestOptions;
}

