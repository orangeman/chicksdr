#include <Arduino.h>


typedef void (*Callback) (String);

void initNostr(Callback func);

void postNote(String text);

void sendDM(String msg);

void subscribeDMs();

void nostrLoop();